 #include "rgbLED.h"

#define IS_RGBW true  // Will use RGBW format
#define NUM_PIXELS 1  // There is 1 WS2812 device in the chain
#define WS2812_PIN 28 // The GPIO pin that the WS2812 connected to

static inline void put_pixel(uint32_t pixel_grb){
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b){
    return ((uint32_t)(r) << 8) |
           ((uint32_t)(g) << 16) |
           (uint32_t)(b);
}

void LED_init(){
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);
}

void LED_toggle(){
    put_pixel(urgb_u32(0x00, 0x00, 0x00));
}

void LED_blue(){
    put_pixel(urgb_u32(0x00, 0x00, 0xB9));
}

void LED_green(){
    put_pixel(urgb_u32(0x00, 0xAC, 0x00));
}

void LED_orange(){
    put_pixel(urgb_u32(0xE1, 0x8D, 0x00));
}

void LED_yellow(){
    put_pixel(urgb_u32(0xE1, 0xCD, 0x00));
}

void LED_red(){
    put_pixel(urgb_u32(0xC0, 0x00, 0x00));
}