# CSU23021-Group43-Assignment

This is a repository of CSU23021 assignment for group 43

# Roles
- Project Workflow Owner - Matthew Wallace  
- GitLab Workflow Owner - Haojun Xing  
- Product Documentation Owner - Austeja Pakulyte  
- Project Demonstration Owner - Andrey Yamkovoy  
- Project Code Owner - Jamie Taylor  
