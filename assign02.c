/**
 * @file assign02.c
 * @author Jamie, Matt, AJ, Andrey
 * @brief Main program for the Morse Code game
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"
#include "rgbLED.h"
#include "morse.h"

void main_asm();
void setup();

int levelNum = 0;
bool levelStarted = false;
int gameIndex;
int timerTime = 1000000000;
int totalCorrect = 0;
int totalIncorrect = 0;
int streak = 0;
int lives = 3;
bool gameOver = false;

char characters[36] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                       'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};

unsigned int morseCharacters[36] = {A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
                                    ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO};

char *words[20] = {"ATTAIN", "BASE", "BITE", "CAT", "DOG", "EDIT", "FOX", "GLUM", "HURT", "JOIN", "PINT",
                   "QUIT", "RACE", "RATS", "SICK", "TOWNS", "USER", "VINE", "YAWN", "ZAP"};

unsigned int morseWords[21] = {ATTAIN, BASE, BITE, CAT, DOG, EDIT, FOX, GLUM, HURT, JOIN, PINT, QUIT, RACE, RATS,
                               SICK, TOWNS, USER, VINE, YAWN, ZAP};

/**
 * @brief prints the morse code equivalent of the random character selected by the parameter
 * @param morse : morse sequence
 */
void printMorse(unsigned int morse)
{
    unsigned int bits;
    while (morse != 0)
    {
        bits = (morse & 0xc0000000) >> 30; // isolate two most significant bits
        if (bits == 0b01)
        {
            printf(".");
        }
        else if (bits == 0b10)
        {
            printf("-");
        }
        else if (bits == 0b11)
        {
            printf(" ");
        }
        morse <<= 2;
    }
    printf("\n");
}

/**
 * @brief helper function for printString - prints character when space is met
 * @param character : morse sequence of character to be printed
 */
void handleSpace(unsigned int character)
{
    int pos;
    int matchFound = false;
    for (int i = 0; i < 36; i++)
    {
        if (character == morseCharacters[i])
        {
            pos = i;
            matchFound = true;
            break;
        }
    }
    if (matchFound)
    {
        printf("%c", characters[pos]);
    }
    else
    {
        printf("?");
    }
}

/**
 * @brief prints the String equivalent of the random character selected by the parameter
 * @param morse : morse sequence
 */
void printString(unsigned int morse)
{
    unsigned int bits;
    unsigned int character = 0;
    while (morse != 0)
    {
        bits = (morse & 0xc0000000) >> 30; // isolate two most significant bits
        if (bits == 0b01 || bits == 0b10)
        {
            character <<= 2;
            character += bits;
        }
        else if (bits == 0b11)
        {
            handleSpace(character);
            character = 0;
        }
        morse <<= 2;
    }
    handleSpace(character);
    printf("\n");
}

/**
 * @brief Generates a random number
 * @param max : max number for rng
 * @return int : 0 <= int <= max
 */
int randomNumber(int max)
{
    srand(time_us_32()); // set pseudo-random seed by taking system time
    return (rand() % max);
}

/**
 * @brief Level selection based on user input
 *
 * @param morse_seq : sequence inputted by user
 */
void letterGot(int morse_seq)
{
    printf("Inputted String: ");
    printString(morse_seq);

    if (levelNum == 0)
    {
        if (morse_seq == ONE)
        {
            // level 1
            LED_green();
            printf("Entering Level 1\nInput the following letters as Morse Code\n");
            levelNum = 1;
            levelStarted = false;
        }
        else if (morse_seq == TWO)
        {
            // level 2
            LED_green();
            printf("Entering Level 2\nInput the following letters as Morse Code\n");
            levelNum = 2;
            levelStarted = false;
        }
        else if (morse_seq == THREE)
        {
            // level 3
            LED_green();
            printf("Entering Level 3\nInput the following words as Morse Code\n");
            levelNum = 3;
            levelStarted = false;
        }
        else if (morse_seq == FOUR)
        {
            // level 4
            LED_green();
            printf("Entering Level 4\nInput the following words as Morse Code\n");
            levelNum = 4;
            levelStarted = false;
        }
        else
        {
            printf("\nIncorrect Input - Select from levels below to play:\n");
            printf("Level 1 = .----\n");
            printf("Level 2 = ..---\n");
            printf("Level 3 = ...--\n");
            printf("Level 4 = ....-\n\n");
            printf("Input: ");
        }
    }
    else
    {
        levelStarted = false;
        if (((levelNum == 1 || levelNum == 2) && morse_seq == morseCharacters[gameIndex]) ||
            ((levelNum == 3 || levelNum == 4) && morse_seq == morseWords[gameIndex]))
        {
            totalCorrect++;
            streak++;
            if (lives < 3)
            {
                printf("You gained a life: Lives: %d\n", ++lives);
            }
            if (streak == 5)
            {
                int total = totalCorrect + totalIncorrect;
                if (levelNum < 4)
                {
                    printf("\nCongrats!!\nTotal Correct: %d\nTotal Incorrect: %d\nAccuracy: %d/%d\n", totalCorrect, totalIncorrect, totalCorrect, total);
                    printf("Moving to Level %d\n", ++levelNum);
                    totalCorrect = totalIncorrect = streak = 0;
                }
                else
                {
                    printf("\nCongrats!!  You Win\nTotal Correct: %d\nTotal Incorrect: %d\nAccuracy: %d/%d\n", totalCorrect, totalIncorrect, totalCorrect, total);
                    gameOver = true;
                }
            }
        }
        else
        {
            totalIncorrect++;
            printf("\nIncorrect input, you lost a life. Lives: %d\n", --lives);
            streak = 0;
            if (lives == 0)
            {
                int total = totalCorrect + totalIncorrect;
                LED_red();
                printf("\nGame Over.\nTotal Correct: %d\nTotal Incorrect: %d\nAccuracy: %d/%d",
                       totalCorrect, totalIncorrect, totalCorrect, total);
                gameOver = true;
            }
        }
    }

    if (levelNum != 0)
    {
        if (lives == 3)
            LED_green();
        else if (lives == 2)
            LED_yellow();
        else if (lives == 1)
            LED_orange();
    }
}
/**
 * @brief Main
 *
 * @return int : Returns 0 if there are no errors
 */
int main()
{
    watchdog_enable(timerTime, 1);

    if (watchdog_caused_reboot())
        printf("\n\nReboot Due to Timeout\n");

    stdio_init_all();
    gpio_set_irq_enabled(21, GPIO_IRQ_EDGE_FALL, true); // ititialise GP21 for falling edge detection
    gpio_set_irq_enabled(21, GPIO_IRQ_EDGE_RISE, true); // ititialise GP21 for rising edge detection
    LED_init();
    LED_blue();
    setup();
    // A welcome header saying morse code game
    printf("\n\n************************************************************************************\n");
    printf("|                                                                                  |\n");
    printf("|  .    . .--. .--.  .-. .---.     .--..--. .--. .---.   .--.    .    .    ..---.  |\n");
    printf("|  |\\  /|:    :|   )(   )|        :   :    :|   :|      :       / \\   |\\  /||      |\n");
    printf("|  | \\/ ||    ||--'  `-. |---     |   |    ||   ||---   | --.  /___\\  | \\/ ||---   |\n");
    printf("|  |    |:    ;|  \\ (   )|        :   :    ;|   ;|      :   | /     \\ |    ||      |\n");
    printf("|  '    ' `--' '   ` `-' '---'     `--'`--' '--' '---'   `--''       `'    ''---'  |\n");
    printf("|                                                                                  |\n");
    printf("************************************************************************************");
    // Introduction to the game
    printf("\n\nMorse Code Game: Group 43\n\nHow to play:\n");
    printf("To play, you must enter the correct morse code sequence\nfor the character or word displayed in the console.");
    printf("\nHold down GP21 for <0.25s for a dot and >0.25s for a dash.\nLeave the button unpressed for 1s for a space and 2s to submit the sequence.");
    printf("\n\nYou have 3 lives before game over.\n\n");
    printf("Select difficulty by entering the corresponding \nMorse Code character for the level number:\n\n");

    printf("Level 1 = .----\n");
    printf("Level 2 = ..---\n");
    printf("Level 3 = ...--\n");
    printf("Level 4 = ....-\n\n");

    while (!gameOver)
    {
        if (!levelStarted)
        {
            levelStarted = true;
            if (levelNum == 1)
            {
                gameIndex = randomNumber(36);
                printf("\n%c  = ", characters[gameIndex]);
                printMorse(morseCharacters[gameIndex]);
            }
            else if (levelNum == 2)
            {
                gameIndex = randomNumber(36);
                printf("\n%c\n", characters[gameIndex]);
            }
            else if (levelNum == 3)
            {
                gameIndex = randomNumber(20);
                printf("\n%s = ", words[gameIndex]);
                printMorse(morseWords[gameIndex]);
            }
            else if (levelNum == 4)
            {
                gameIndex = randomNumber(20);
                printf("\n%s\n", words[gameIndex]);
            }
            printf("Input: ");
        }
    }

    // Returning zero indicates everything went okay.
    return 0;
}