/**
 * @file rgbLED.h
 * @author AJ Pakulyte 
 * @brief Provides functionailty for RGB LED
 */

#include "hardware/pio.h"
#include "hardware/gpio.h"
#include "rgbLED.pio.h"

/**
 * @brief Initialise LED
 */
void LED_init();

/**
 * @brief Toggle LED
 */
void LED_toggle();

/**
 * @brief Toggle LED - blue (default)
 */
void LED_blue();

/**
 * @brief Toggle LED - green (3 lives)
 */
void LED_green();

/**
 * @brief Toggle LED - yellow (2 lives)
 */
void LED_yellow();

/**
 * @brief Toggle LED - orange (1 life)
 */
void LED_orange();

/**
 * @brief Toggle LED - red (0 lives)
 */
void LED_red();
